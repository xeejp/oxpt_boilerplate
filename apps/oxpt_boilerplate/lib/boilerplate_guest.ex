defmodule Oxpt.Boilerplate.Guest do
  @moduledoc """
  Documentation for Oxpt.Boilerplate.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter, Saga}
  alias Oxpt.JoinGame
  alias Oxpt.Persistence
  alias Oxpt.Game
  alias Oxpt.Player
  alias Oxpt.Lounge.Host.AddedDummyGuest

  alias Oxpt.Boilerplate.{Host, Locales}

  alias Oxpt.Boilerplate.Events.{
    FetchState,
    UpdateState,
    UpdateStateAll,
    Read,
    ChangePage,
    ChangeSetting,
    ChangeShared,
    ChangeOwn,
    ChangePage
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{room_id: _room_id}, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_boilerplate",
      category: "category_other"
    }

  @impl true
  def spawn(%__MODULE__{room_id: room_id}) do
    id = Saga.self()
    Persistence.Game.setup(id, room_id)

    host_game_id =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(%Start{saga: Host.new(room_id, game_id: id)})
      else
        nil
      end

    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %FetchState{game_id: ^id} -> true end),
          Filter.new(fn %AddedDummyGuest{game_id: ^id} -> true end),
          Filter.new(fn %ChangePage{game_id: ^id} -> true end),
          Filter.new(fn %Read{game_id: ^id} -> true end),
          Filter.new(fn %ChangeSetting{game_id: ^id} -> true end),
          Filter.new(fn %ChangeShared{game_id: ^id} -> true end),
          Filter.new(fn %ChangeOwn{game_id: ^id} -> true end),
          Filter.new(fn %JoinGame{game_id: ^id} -> true end)
        ])
    })

    initial_state = %{
      room_id: room_id,
      host_game_id: host_game_id,
      host_guest_id: nil,
      guest_game_id: id,
      locales: Locales.get(),
      players: %{},
      shared: "A",
      page: "instruction",
      dummy_guest_list: []
    }

    {:loop, initial_state}
  end

  @impl true
  def yield({:loop, state}) do
    event = perform(%Receive{})

    new_state = handle_event_body(event.body, state)
    {:loop, new_state}
  end

  def handle_event_body(%JoinGame{guest_id: guest_id, host: true}, state) do
    perform(%Dispatch{
      body: %JoinGame{game_id: state.host_game_id, guest_id: guest_id, host: true}
    })

    %{state | host_guest_id: guest_id}
  end

  def handle_event_body(%JoinGame{guest_id: guest_id}, state) do
    player = new_player(state)
    new_state = put_in(state, [:players, guest_id], player)

    case state.page do
      "instruction" ->
        perform(%Dispatch{
          body: %UpdateState{
            game_id: Saga.self(),
            guest_id: guest_id,
            state: %{
              players: %{guest_id => player}
            }
          }
        })

        # ignored when state.host_guest_id is nil.
        perform(%Dispatch{
          body: %UpdateState{
            game_id: Saga.self(),
            guest_id: state.host_guest_id,
            state: %{
              players: %{guest_id => player}
            }
          }
        })

      _ ->
        perform(%Dispatch{
          body: %UpdateStateAll{
            game_id: Saga.self(),
            state: %{
              players: %{guest_id => player}
            }
          }
        })
    end

    new_state
  end

  def handle_event_body(%FetchState{guest_id: guest_id}, state) do
    perform(%Dispatch{
      body: %UpdateState{
        game_id: Saga.self(),
        guest_id: guest_id,
        state: Map.put(state, :guest_id, guest_id)
      }
    })

    state
  end

  def handle_event_body(%AddedDummyGuest{dummy_guest_list: dummy_guest_list}, state) do
    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: Saga.self(),
        state: %{
          dummy_guest_list: dummy_guest_list
        }
      }
    })

    put_in(state.dummy_guest_list, dummy_guest_list)
  end

  def handle_event_body(%Read{guest_id: guest_id}, state) do
    perform(%Dispatch{
      body: %UpdateState{
        game_id: Saga.self(),
        guest_id: guest_id,
        state: %{
          players: %{
            guest_id => %{
              read: true,
              status: "instruction finished"
            }
          }
        }
      }
    })

    # ignored when state.host_guest_id is nil.
    perform(%Dispatch{
      body: %UpdateState{
        game_id: Saga.self(),
        guest_id: state.host_guest_id,
        state: %{
          players: %{
            guest_id => %{
              read: true,
              status: "instruction finished"
            }
          }
        }
      }
    })

    state
    |> put_in([:players, guest_id, :read], true)
    |> put_in([:players, guest_id, :status], "instruction finished")
  end

  def handle_event_body(%ChangePage{page: page, request_id: request_id}, state) do
    previous_page = state.page

    state =
      case {previous_page, page} do
        {"experiment", "instruction"} ->
          perform(%Dispatch{
            body: %UpdateStateAll{
              game_id: Saga.self(),
              event: "reset_players_state"
            }
          })

          reset_players_state(state)
          |> Map.put(:shared, "A")

        _ ->
          state
      end
      |> Map.put(:page, page)
      |> set_status(page)

    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: Saga.self(),
        state: %{
          page: page,
          shared: Map.get(state, :shared)
        }
      }
    })

    response(request_id)
    state
  end

  def handle_event_body(%ChangeSetting{payload: payload, request_id: request_id}, state) do
    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: Saga.self(),
        state: %{
          locales: %{
            en: %{
              translations: %{
                variables: payload["locales_temp"]["en"]
              }
            },
            ja: %{
              translations: %{
                variables: payload["locales_temp"]["ja"]
              }
            }
          }
        }
      }
    })

    response(request_id)
    update_locales(state, payload["locales_temp"])
  end

  def handle_event_body(%ChangeShared{value: value}, state) do
    perform(%Dispatch{
      body: %UpdateStateAll{
        game_id: Saga.self(),
        state: %{
          shared: value
        }
      }
    })

    Map.put(state, :shared, value)
  end

  def handle_event_body(
        %ChangeOwn{guest_id: guest_id, value: value, request_id: request_id},
        state
      ) do
    perform(%Dispatch{
      body: %UpdateState{
        game_id: Saga.self(),
        guest_id: guest_id,
        state: %{
          players: %{guest_id => %{value: value}}
        }
      }
    })

    # ignored when state.host_guest_id is nil.
    perform(%Dispatch{
      body: %UpdateState{
        game_id: Saga.self(),
        guest_id: state.host_guest_id,
        state: %{
          players: %{guest_id => %{value: value}}
        }
      }
    })

    response(request_id)
    put_in(state.players[guest_id].value, value)
  end

  def reset_players_state(state) do
    update_in(state.players, fn players ->
      Enum.map(players, fn {guest_id, player_state} ->
        player_state =
          player_state
          |> Map.merge(new_player(state))
          |> Map.put(:joined, true)

        {guest_id, player_state}
      end)
      |> Enum.into(%{})
    end)
  end

  def set_status(state, page) do
    update_in(state.players, fn players ->
      Enum.map(players, fn {guest_id, player_state} ->
        player_state =
          player_state
          |> update_in([:status], fn status ->
            case page do
              "experiment" -> "experiment"
              _ -> status
            end
          end)

        {guest_id, player_state}
      end)
      |> Enum.into(%{})
    end)
  end

  def update_locales(state, locales_temp) do
    # localeを変更
    Enum.reduce(Map.keys(locales_temp), state, fn key, acc_state ->
      lang_temp = locales_temp[key]

      update_in(
        acc_state,
        [:locales, String.to_existing_atom(key), :translations],
        fn translations ->
          Enum.reduce(Map.keys(lang_temp), translations, fn trans_key, acc_trans ->
            put_in(
              acc_trans,
              [:variables, String.to_existing_atom(trans_key)],
              lang_temp[trans_key]
            )
          end)
        end
      )
    end)
  end

  def new_player(state) do
    %{
      value: "A",
      status: "reading",
      read: false,
      joined: state.page != "experiment" and state.page != "result"
    }
  end

  defp response(request_id, status \\ :ok, payload \\ %{}) do
    send(elem(request_id, 0), {:response, request_id, payload})
    :ok
  end
end
