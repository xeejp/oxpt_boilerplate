defmodule Oxpt.Boilerplate.Guest.PlayerSocket do
  defstruct [:game_id, :guest_id, :channel_pid]

  use Oxpt.PlayerSocket

  alias Cizen.{Dispatcher, Filter, Event}
  alias Oxpt.Player.{Input, Output, Request}

  alias Oxpt.Boilerplate.Events.{
    UpdateState,
    UpdateStateAll,
    FetchState,
    Read,
    ChangeShared,
    ChangeOwn
  }

  require Filter

  @impl GenServer
  def init(%__MODULE__{game_id: game_id, guest_id: guest_id} = socket) do
    Dispatcher.listen(
      Filter.any([
        Filter.new(fn %UpdateStateAll{game_id: ^game_id} -> true end),
        Filter.new(fn %UpdateState{game_id: ^game_id, guest_id: ^guest_id} ->
          true
        end)
      ])
    )

    Dispatcher.dispatch(%FetchState{
      game_id: game_id,
      guest_id: guest_id
    })

    state = %{
      throttled?: false,
      updated?: false,
      state: %{}
    }

    {:ok, socket |> Map.from_struct() |> Map.merge(state)}
  end

  @impl GenServer
  def handle_info(%event_type{event: event, state: next_state}, state)
      when event_type in [UpdateStateAll, UpdateState] do
    state = put_in(state.state, next_state)
    state = put_in(state.updated?, true)
    {:noreply, state, {:continue, :try_send}}
  end

  @impl GenServer
  def handle_info(:end_of_throttled_interval, state) do
    state = put_in(state.throttled?, false)

    {:noreply, state, {:continue, :try_send}}
  end

  @impl GenServer
  def handle_info({:response, _, _}, state), do: {:noreply, state}

  @impl GenServer
  def handle_continue(:try_send, state) do
    state =
      if state.updated? and not state.throttled? do
        send(
          state.channel_pid,
          %Output{
            event: "update_state",
            payload: %{state: state.state}
          }
        )

        state = put_in(state.throttled?, true)
        state = put_in(state.updated?, false)
        # 30fps
        Process.send_after(self(), :end_of_throttled_interval, 33)
        state
      else
        state
      end

    {:noreply, state}
  end

  @impl GenServer
  def handle_cast(%Input{event: "read"}, state) do
    Dispatcher.dispatch(%Read{
      game_id: state.game_id,
      guest_id: state.guest_id
    })

    {:noreply, state}
  end

  def handle_cast(%Input{event: "change shared", payload: value}, state) do
    Dispatcher.dispatch(%ChangeShared{
      game_id: state.game_id,
      value: value
    })

    {:noreply, state}
  end

  def handle_call(%Request{event: "change own", payload: value, timeout: timeout}, from, state) do
    request_id = {self(), from}

    Dispatcher.dispatch(%ChangeOwn{
      game_id: state.game_id,
      guest_id: state.guest_id,
      value: value,
      request_id: request_id
    })

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, state}
    after
      timeout ->
        {:noreply, state}
    end
  end
end
