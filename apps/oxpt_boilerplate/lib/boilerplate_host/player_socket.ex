defmodule Oxpt.Boilerplate.Host.PlayerSocket do
  defstruct [:game_id, :guest_game_id, :guest_id, :channel_pid]

  use Oxpt.PlayerSocket

  alias Cizen.{Dispatcher, Filter, Event}
  alias Oxpt.Player.{Output, Request}

  alias Oxpt.Boilerplate.Events.{
    UpdateState,
    UpdateStateAll,
    ChangePage,
    FetchState,
    ChangeSetting
  }

  require Filter

  @impl GenServer
  def init(%__MODULE__{} = socket) do
    %{guest_game_id: guest_game_id, guest_id: guest_id} = socket

    Dispatcher.listen(
      Filter.any([
        Filter.new(fn %UpdateStateAll{game_id: ^guest_game_id} ->
          true
        end),
        Filter.new(fn %UpdateState{game_id: ^guest_game_id, guest_id: ^guest_id} ->
          true
        end)
      ])
    )

    Dispatcher.dispatch(%FetchState{
      game_id: guest_game_id,
      guest_id: guest_id
    })

    {:ok, socket}
  end

  @impl GenServer
  def handle_info(%event_type{event: event, state: state}, socket)
      when event_type in [UpdateStateAll, UpdateState] do
    send(
      socket.channel_pid,
      %Output{
        event: event,
        payload: %{state: state}
      }
    )

    {:noreply, socket}
  end

  @impl GenServer
  def handle_info({:response, _, _}, state), do: {:noreply, state}

  @impl GenServer
  def handle_call(%Request{event: "change page", payload: page, timeout: timeout}, from, socket) do
    request_id = {self(), from}

    Dispatcher.dispatch(%ChangePage{
      game_id: socket.guest_game_id,
      page: page,
      request_id: request_id
    })

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, socket}
    after
      timeout ->
        {:noreply, socket}
    end
  end

  def handle_call(%Request{event: "setting", payload: payload, timeout: timeout}, from, socket) do
    request_id = {self(), from}

    Dispatcher.dispatch(%ChangeSetting{
      game_id: socket.guest_game_id,
      payload: payload,
      request_id: request_id
    })

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, socket}
    after
      timeout ->
        {:noreply, socket}
    end
  end
end
