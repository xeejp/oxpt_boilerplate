defmodule Oxpt.Boilerplate.Host do
  @moduledoc """
  Documentation for Oxpt.Boilerplate.Host
  """

  use Cizen.Automaton
  defstruct [:room_id, :guest_game_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Oxpt.Persistence

  alias Cizen.{Event, Filter, Saga}
  alias Oxpt.Player.{Input, Request}
  alias Oxpt.Boilerplate.Events.{UpdateStateAll, UpdateState}

  alias Oxpt.GetLog

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../host") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{guest_game_id: guest_game_id}, guest_id) do
    %__MODULE__.PlayerSocket{
      game_id: game_id,
      guest_id: guest_id,
      guest_game_id: guest_game_id
    }
  end

  @impl Oxpt.Game
  def new(room_id, params) do
    %__MODULE__{room_id: room_id, guest_game_id: params[:game_id]}
  end

  @impl true
  def spawn(%__MODULE__{room_id: room_id, guest_game_id: guest_game_id}) do
    id = Saga.self()
    Persistence.Game.setup(id, room_id)

    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %Input{game_id: game_id} ->
            game_id == id or game_id == guest_game_id
          end),
          Filter.new(fn %UpdateStateAll{game_id: ^guest_game_id} ->
            true
          end),
          Filter.new(fn %UpdateState{game_id: ^guest_game_id} ->
            true
          end),
          Filter.new(fn %Request{game_id: game_id} ->
            game_id == id or game_id == guest_game_id
          end),
          Filter.new(fn %GetLog{game_id: ^id} -> true end)
        ])
    })

    initial_state = %{
      log: []
    }

    {:loop, initial_state}
  end

  @impl true
  def yield({:loop, state}) do
    event = perform(%Receive{})

    state =
      case event.body do
        %GetLog{} ->
          perform(%Dispatch{
            body: %GetLog.Response{
              request_id: event.id,
              log: ["Timestamp\tEvent"] ++ state.log
            }
          })

          state

        event_body ->
          update_in(state, [:log], fn log ->
            [
              "#{Date.utc_today()}_#{Time.utc_now()}\t#{inspect(event_body)}"
            ] ++ log
          end)
      end

    {:loop, state}
  end
end
