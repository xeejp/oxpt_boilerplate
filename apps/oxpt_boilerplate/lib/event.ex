defmodule Oxpt.Boilerplate.Events do
  @moduledoc """
  Documentation for Oxpt.Boilerplate.Events

  gameで使うイベントを定義する
  """

  defmodule UpdateStateAll do
    defstruct [:game_id, event: "update_state", state: %{}]
  end

  defmodule UpdateState do
    defstruct [:guest_id, :game_id, event: "update_state", state: %{}]
  end

  defmodule FetchState do
    defstruct [:game_id, :guest_id]
  end

  defmodule Read do
    defstruct [:game_id, :guest_id]
  end

  defmodule ChangePage do
    defstruct [:game_id, :page, :request_id]
  end

  defmodule ChangeSetting do
    defstruct [:game_id, :payload, :request_id]
  end

  defmodule ChangeShared do
    defstruct [:game_id, :value]
  end

  defmodule ChangeOwn do
    defstruct [:game_id, :guest_id, :value, :request_id]
  end
end
