import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import IconButton from '@material-ui/core/IconButton'
import SettingsIcon from '@material-ui/icons/Settings'

import i18nInstance from '../i18n'

export default () => {
  const { locales, page, requestState } = useStore()

  if (!(locales))
    return <></>

  const [t, i18n] = useTranslation('translations', { i18nInstance })

  const languageVariablesObject = (key) => {
    return (typeof locales[key].translations.variables) === 'object' ? locales[key].translations.variables : {}
  }

  const langKeys = Object.keys(locales)
  const initUnits = langKeys.reduce((units, key) => ({
    ...units,
    [key]: languageVariablesObject(key)
  }), {})
  const [open, setOpen] = useState(page === 'instruction' || page === 'waiting')
  var localesTempStr = JSON.stringify(initUnits)
  var localesTemp = JSON.parse(localesTempStr)

  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleVariable = (obj, e) => {
    var tempvar = obj.var
    localesTemp[obj.lang][tempvar] = e.target.value
  }

  const handleOnCancel = () => {
    setOpen(false)
  }

  const handleOnSend = () => {
    requestState({
      event: 'setting',
      payload: {
        locales_temp: localesTemp
      },
      callback: () => {}
    })
    setOpen(false)
  }

  return (
    <>
      <IconButton aria-label={t('host.setting.title_01')} value="setting" disabled={page !== 'instruction' && page !== 'waiting'} onClick={setOpen.bind(null, true)}>
        <SettingsIcon />
      </IconButton>
      <Dialog
        onClose={setOpen.bind(null, false)}
        open={open}
        fullWidth={true}
        maxWidth="xl">
        <DialogTitle>{t('host.setting.title_01')}</DialogTitle>
        <DialogContent>
          <TableContainer component={Paper}>
            <Table size="small">
              <TableHead>
                <TableRow>
                  {Object.keys(initUnits).map(k => {
                    return <TableCell align="center" key={k}>{t('host.setting.' + k + '_01')}</TableCell>
                  })}
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(initUnits['en']).map((key, index) => {
                  return (<TableRow key={key + '_' + index}>
                    {Object.keys(initUnits).map(j => {
                      return <TableCell align="center" key={key + '_' + index + '_' + j}><TextField
                        key={key + '_' + index + '_' + j}
                        defaultValue={Object.values(localesTemp[j])[index] || null}
                        onChange={handleVariable.bind(null, { lang: j, var: key })}
                      /></TableCell>
                    })}
                  </TableRow>)
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleOnCancel}>
            {t('host.setting.cancel_01')}
          </Button>
          <Button onClick={handleOnSend} color="primary" disabled={page !== 'instruction' && page !== 'waiting'}>
            {t('host.setting.send_01')}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
