import { createAction, handleActions } from 'redux-actions'
import merge from 'deepmerge'
import { fromSnakeToCamel } from '../util'
export const defaultState = {
}

export const PUSH_STATE = 'PUSH_STATE'
export const REQUEST_STATE = 'REQUEST_STATE'
export const UPDATE_STATE = 'UPDATE_STATE'
export const RECEIVE_RESPONSE = 'RECEIVE_RESPONSE'
export const RECEIVE_ERROR = 'RECEIVE_ERROR'
export const RECEIVE_TIMEOUT = 'RECEIVE_TIMEOUT'
export const RESET_PLAYERS_STATE = 'RESET_PLAYERS_STATE'
export const pushState = createAction(PUSH_STATE)
export const requestState = createAction(REQUEST_STATE)
export const updateState = createAction(UPDATE_STATE)
export const receiveResponse = createAction(RECEIVE_RESPONSE)
export const receiveError = createAction(RECEIVE_ERROR)
export const receiveTimeout = createAction(RECEIVE_TIMEOUT)
export const resetPlayersState = createAction(RESET_PLAYERS_STATE)

function aliasPlayerState(state) {
  const { guestId, players } = state
  if (!guestId || !players) {
    return state
  }
  const playerState = players[guestId] || {}
  return { ...state, ...playerState }
}

export const reducer = handleActions(
  {
    [PUSH_STATE]: (state, _action) => state,
    [REQUEST_STATE]: (state, _action) => state,
    [RECEIVE_RESPONSE]: (state, action) => {
      if (action.payload) {
        return merge(state, fromSnakeToCamel(action.payload), { arrayMerge: (_destArr, srcArr) => srcArr })
      } else {
        return state
      }
    },
    [RECEIVE_ERROR]: (state, _action) => state,
    [RECEIVE_TIMEOUT]: (state, _action) => state,
    [UPDATE_STATE]: (state, action) => {
      const newState = merge(state, fromSnakeToCamel(action.payload), { arrayMerge: (_destArr, srcArr) => srcArr })
      return aliasPlayerState(newState)
    },
    [RESET_PLAYERS_STATE]: (state, _action) => {
      const players = Object.keys(state.players).reduce((players, player) => {
        players[player] = {
          value: 'A',
          status: 'reading',
          read: false,
          joined: true
        }
        return players
      }, {})
      return aliasPlayerState({ ...state, players })
    }
  },
  defaultState
)
