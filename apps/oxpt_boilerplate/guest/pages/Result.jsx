import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Grid from '@material-ui/core/Grid'
import Hidden from '@material-ui/core/Hidden'

import i18nInstance from '../i18n'
import Chart from '../components/Chart'

export default () => {
  const { locales } = useStore()

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  return (
    <>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Hidden xsDown><Grid item sm={3}></Grid></Hidden>
        <Grid item xs={12} sm={6}>
          <Chart />
        </Grid>
        <Hidden xsDown><Grid item sm={3}></Grid></Hidden>
      </Grid>
    </>
  )
}
