import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import FormLabel from '@material-ui/core/FormLabel'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2),
  },
  contents: {
    width: 300,
    margin: 'auto'
  },
  formControl: {
    margin: theme.spacing(3),
  },
  group: {
    margin: theme.spacing(1, 0),
  },
}))

export default () => {
  const [enableOwn, setEnableOwn] = useState(true);
  const classes = useStyles()
  const { locales, shared, value, pushState, requestState } = useStore()
  if (!(locales && shared && value))
    return <></>

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const numRound = 1

  const plural = (n, unit) => {
    if (n > 1) {
      return unit + '_m'
    } else {
      return unit + '_s'
    }
  }

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const onChangeShared = e => {
    pushState({ event: 'change shared', payload: e.target.value })
  }

  const onChangeOwn = e => {
    setEnableOwn(false);
    requestState({ event: 'change own', payload: e.target.value, successCallback: () => setEnableOwn(true), timeoutCallback: () => setEnableOwn(true) });
  }

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  return (
    <>
      <p>{t(plural(numRound, 'guest.experiment.instruction_01'), { ...variablesObject(), num: numRound })}</p>
      <Grid item xs={12} className={classes.contents}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">{t('variables.shared_01')}</FormLabel>
          <RadioGroup
            name="shared"
            className={classes.group}
            value={shared}
            onChange={onChangeShared}
          >
            <FormControlLabel value="A" control={<Radio />} label="A" />
            <FormControlLabel value="B" control={<Radio />} label="B" />
            <FormControlLabel value="C" control={<Radio />} label="C" />
          </RadioGroup>
        </FormControl>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">{t('variables.own_01')}</FormLabel>
          <RadioGroup
            name="own"
            className={classes.group}
            value={value}
            onChange={onChangeOwn}
          >
            <FormControlLabel value="A" control={<Radio disabled={!enableOwn} />} label="A" />
            <FormControlLabel value="B" control={<Radio disabled={!enableOwn} />} label="B" />
            <FormControlLabel value="C" control={<Radio disabled={!enableOwn} />} label="C" />
          </RadioGroup>
        </FormControl>
      </Grid>
    </>
  )
}
