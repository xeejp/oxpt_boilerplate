import { createStore, applyMiddleware } from 'redux'
import createSagaMiddelware from 'redux-saga'
import { reducer } from '../actions'
import root from '../saga'

export default function configureStore() {
  const sagaMiddleware = createSagaMiddelware()
  const middlewares = [sagaMiddleware]
  if (process.env.NODE_ENV === `development`) {
    const { logger } = require(`redux-logger`)
    middlewares.push(logger)
  }
  let store = createStore(reducer, applyMiddleware(...middlewares))
  sagaMiddleware.run(root)
  return store
}
